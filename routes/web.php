<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');


//Route::post('/contact','UsersController@contact')->name('contact');

	Route::get('login', 'PageController@showLoginForm')->name('login');
	Route::post('login', 'PageController@login')->name('login_action');
		//Route::post('/hash', 'RegisterController@hash');



Route::group(['middleware'=>'auth:web'], function(){
		Route::get('users', 'UsersController@index')->name('users');
		Route::get('user/{user}', 'UsersController@show');
		Route::get('user/{user}/edit','UsersController@edit');
		Route::put('user/{user}', 'UsersController@update')->name('user.update');
		Route::get('user/{user}/delete', 'UsersController@destroy');
		Route::get('profile', 'UsersController@profile')->name('profile');
		Route::get('transfer', 'TransactionController@transfer');
		Route::post('transfer', 'TransactionController@save');
		Route::get('transactions', 'TransactionController@all');
		Route::get('all-transactions', 'TransactionController@all_trans')->name('transactions');
		Route::post('change-status', 'TransactionController@change_status');
		Route::get('status', 'StatusController@index')->name('status');
		Route::post('status', 'StatusController@store');
		Route::get('logout', 'UsersController@logout')->name('logout');

		//registration routes
	Route::group(['namespace'=>'Auth'], function(){
		Route::get('register', 'RegisterController@showRegistrationForm');
		Route::post('register', 'RegisterController@register');
	});

});
