<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="{{asset('images/fav.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Admin | View Profile | {{ $userInfo->name }}
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('assets/css/paper-dashboard.css?v=2.0.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css" rel="stylesheet')}}" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="{{asset('assets/img/logo-small.png')}}">
          </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Rabo Bank
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>

    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">Rabo Bank Customer Account</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">

            <div style="margin-right: 20px; color: #f13a11;"><b>Status Message:</b> </div>
           <form method="post" action="/status">
              <div class="input-group no-border">
                <select name="msg_code" class="">
                  <option class="form-control" value="">--Select Success Message--</option>
                  <option value="0" {{$status->msg_code == 0 ? 'selected':'' }}>Success</option>
                  <option value="1" {{$status->msg_code == 1 ? 'selected':'' }}>Pending</option>
                  <option value="2" {{$status->msg_code == 2 ? 'selected':'' }}>Tax Code Request</option>
                  <option value="3" {{$status->msg_code == 3 ? 'selected':'' }}>Waiting</option>
                  <option value="4" {{$status->msg_code == 4 ? 'selected':'' }}>On Hold</option>
                  <option value="5" {{$status->msg_code == 5 ? 'selected':'' }}>Remita Number</option>
                </select>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <button><i class="nc-icon nc-send"></i></button>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">

              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bank" style="font-size: 30px;"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Account Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="/register">Register User</a>
                  <a class="dropdown-item" href="/users">Users</a>
                  <!--<a class="dropdown-item" href="/status">Toggle Transfer Message</a>-->
                  <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <style type="text/css">
                .nav-link:hover, .cust:hover{
                  color: #f13a11 !important;
                  text-decoration: none;

                }
                input{
                  text-transform: none !important;
                }
              </style>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-sm">


</div> -->
      <div class="content">
        <div class="row">
          <div class="col-md-4">

            <div class="card card-user">
              <div class="image">
                <img src="../assets/img/damir-bosnjak.jpg" alt="...">
              </div>
              <div class="card-body">
                <div class="author">
                  <div class="cust">
                    <img class="avatar border-gray" src="{{ asset('uploads/'.$userInfo->filename) }}" alt="...">
                    <h5 class="title" style="color: #f13a11;">Customer Information</h5>
                  </div>
                   <p class="description">
                    <p>Name:</p><h6 class="title"> {{ $userInfo->name }} </h6>
                    <p>Email Address:</p><h6 class="title"> {{ $userInfo->email }} </h6>
                    <p>Phone:</p><h6 class="title"> {{ $userInfo->phone }} </h6>
                  </p>
                </div>

              </div>
              <div class="card-footer">
                <hr>
                <div class="button-container">
                  <div class="row">


                    <div style="margin-left: 30px;" class="col-lg-10 mr-auto">
                      <h1>${{ number_format($userInfo->acct_amount) }}
                        <br>
                        <small>Balance</small>
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header">

              </div>
              <div class="card-body">

              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5 style="color: #f13a11;">Transaction History</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    @if(count($trans) > 0)
                    @foreach($trans as $tran)
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label><b>Date</b></label>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value=" {{ date('j F, Y | h:i:s A', strtotime($tran->created_at)) }} ">
                        <label><b>Amount:</b></label>
                        <p><span style="color: red; margin-left: 100px; font-size: 15px;"> - ${{ number_format($tran->trans_amount) }}</span></p>
                        <label><b>Sent To:</b></label>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value="{{ $tran->receiver_acct_no}} ">
                        <label><b>Recepient Bank:</b></label>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value="{{ $tran->receiver_bank }} ">
                        <label><b>Transaction Status:</b></label>
                        <input type="text" class="form-control" disabled=""  value="{{ $tran->trans_status }} ">
                        {{--{{ $tran->created_at->diffForHumans() }} --}}
                      </div>
                    </div>
                    @endforeach
                    <div class="row">
                    <div class="update ml-auto mr-auto">
                      <a href="/transfer"><button type="submit" class="btn btn-primary btn-round" style="margin-left: 250px; background-color: #f13a11 !important;">View all</button></a>

                    </div>
                  </div>
                    @else
                    <h6 style="margin-left: 200px; text-transform: none;"><i>No transaction history yet.</i></h6>
                    @endif


                </form>
              </div>
            </div>
          </div>
        </div>
      </div>


      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="" >About Us</a>
                </li>
                <li>
                  <a href="" >Reach Out</a>
                </li>
                <li>
                  <a href="/transfer" >Tranfer</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, Rabo Bank <i class="fa fa-heart heart"></i> - All Righs Reserved
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('assets/js/paper-dashboard.min.js?v=2.0.0')}}" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('assets/demo/demo.js')}}"></script>
</body>

</html>
