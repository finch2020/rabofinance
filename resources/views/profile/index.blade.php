<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="{{asset('images/fav.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Profile | {{ $userInfo->name }}
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('assets/css/paper-dashboard.css?v=2.0.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css" rel="stylesheet')}}" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="{{asset('images/fav.png')}}">
          </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal" style="font-size: 20px;">
          Rabo Bank
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      
      <div class="sidebar-wrapper">
        <ul class="nav">
          <img src="{{ asset('images/side.png') }}" alt="...">
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">Rabo Bank Customer Account</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
           
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="/transfer">
                  <p><b>Transfer Funds</b></p>
                  <p>
                    <span class="d-lg-none d-md-block"></span>
                  </p>
                </a>
              </li> 
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="/logout">
                  <p><b>Logout</b></p>
                  <p>
                    <span class="d-lg-none d-md-block"></span>
                  </p>
                </a>
              </li>
              <style type="text/css">
                .nav-link, .cust:hover{
                  color: #f13a11 !important;
                  text-decoration: none;

                }
              </style>
              <!--<li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bank" style="font-size: 30px;"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Account Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="/transfer">Transfer Funds</a>
                  <a class="dropdown-item" href="/logout">Logout</a>
                </div>
              </li>-->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-sm">
  
  
</div> -->
      <div class="content">
        <div class="row">
          <div class="col-md-4">
        @if(Session::has('Unauthorised'))
          <div class="alert alert-danger alert-with-icon alert-dismissible fade show">
            <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                  <i class="nc-icon nc-simple-remove"></i>
            </button>
              <span>
                <span data-notify="icon" class="nc-icon nc-bell-55"></span>
                  {{ Session::get('Unauthorised') }}
              </span>
      </div>
        @endif

        @if(Session::has('flash_msg'))
          <div class="alert alert-success alert-dismissible fade show">
            <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                  <i class="nc-icon nc-simple-remove"></i>
            </button>
              <span>
                  {{ Session::get('flash_msg') }}
              </span>
      </div>
        @endif 
            <div class="card card-user">
              <div class="image">
                <img src="{{ asset('images/base.png') }}" alt="...">
              </div>
              <div class="card-body">
                <div class="author">
                  <div class="cust">
                    <img class="avatar border-gray" src="{{ asset('uploads/'.$userInfo->filename) }}" alt="...">
                    <h5 class="title" style="color: #f13a11;">Customer Information</h5>
                  </div>
                   <p class="description">
                    <p>Name:</p><h6 class="title"> {{ $userInfo->name }} </h6>
                    <p>Email Address:</p><h6 class="title"> {{ $userInfo->email }} </h6>
                    <p>Phone:</p><h6 class="title"> {{ $userInfo->phone }} </h6> 
                  </p>
                </div>
                
              </div>
              <div class="card-footer">
                <hr>
                <div class="button-container">
                  <div class="row">
                    
                    
                    <div style="margin-left: 30px;" class="col-lg-10 mr-auto">
                      <h1>&euro;{{ number_format($userInfo->acct_amount) }}
                        <br>
                        <small>Balance</small>
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              
              <div class="card-body">
                
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5>Transaction History</h5>
              </div>
              <div class="card-body">
                <form>
                  <div class="row">
                    @if(count($trans) > 0)
                    @foreach($trans as $tran)
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <div style="border-bottom: 2px solid #f13a11; box-sizing: border-box;"></div>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value=" {{ date('j F, Y | h:i:s A', strtotime($tran->created_at)) }} ">
                        <label><b>Amount:</b></label>
                        <p><span style="color: red; margin-left: 100px; font-size: 15px;"> - &euro;{{ number_format($tran->trans_amount) }}</span></p>
                        <label><b>Sent To:</b></label>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value="{{ $tran->receiver_acct_no}} | {{$tran->f_name}} {{$tran->l_name}} ">
                        <label><b>Recepient Bank:</b></label>
                        <input type="text" class="form-control" disabled=""  value="{{ $tran->receiver_bank }} ">
                        <label><b>Transaction Status:</b></label>
                        <input type="text" class="form-control" disabled=""  value="{{ $tran->trans_status }} ">
                        
                        {{--{{ $tran->created_at->diffForHumans() }} --}}
                      </div>
                    </div>
                  </form>
                    @endforeach
                    <div class="row">
                    <div class="update ml-auto mr-auto">
                      <a href="/transactions"><div class="btn btn-primary btn-round" style="margin-left: 250px; background-color: #f13a11 !important;">View all</div></a>
                      
                    </div>
                  </div>
                    @else
                    <h6 style="margin-left: 200px; text-transform: none;"><i>No transaction history yet.</i></h6>
                    @endif         
                  
              </div>
            </div>
             <img src="{{ asset('images/base.png') }}">
          </div>
        </div>
      </div>

      
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="" >About Us</a>
                </li>
                <li>
                  <a href="" >Reach Out</a>
                </li>
                <li>
                  <a href="/transfer" >Tranfer</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, Rabo Bank <i class="fa fa-heart heart"></i> - All Righs Reserved
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('assets/js/paper-dashboard.min.js?v=2.0.0')}}" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('assets/demo/demo.js')}}"></script>
</body>

</html>