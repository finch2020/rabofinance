<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-all-users', function($user){
            return $user->isAdmin === 1;
        });

        Gate::define('view-user', function($user){
            return $user->isAdmin === 1;
        });

        Gate::define('edit-user', function($user){
            return $user->isAdmin === 1;
        });

        Gate::define('delete-user', function($user){
            return $user->isAdmin === 1;
        });

        Gate::define('view-status', function($user){
            return $user->isAdmin === 1;
        });

        Gate::define('can-register', function($user){
            return $user->isAdmin === 1;
        });
        
    }
}
