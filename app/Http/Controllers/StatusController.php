<?php

namespace App\Http\Controllers;

use App\Status;
use Session;
use Gate;
use Illuminate\Http\Request;
use Redirect;

class StatusController extends Controller
{
    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
        if (Gate::allows('view-status')) {
            return view('status.list');
        }
        Session::flash('flash_msg', 'You are not authorised to perform that action.');
        //Session::flash(['flash_type', 'error_msg']);
        return redirect::route('profile');
    }

    public function store(Request $request)
    {
        if (Status::where('id',1)->update($request->all()) ) {
            Session::flash('flash_msg', 'Status message changed.');
            //Session::flash(['flash_type', 'success_msg']);

            return redirect::route('users');
        }

        Session::flash('flash_msg', 'Unable to change the status message');
        //Session::flash('flash_type', 'error_msg');

        return redirect::route('status');
    }

    
    public function destroy(Status $status)
    {
        //
    }
}
