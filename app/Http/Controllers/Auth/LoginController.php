<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Validator;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Mail;
use App\Mail\regNotify;
use App\Mail\loginNotify;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request){
        return view('auth.login');
    }


    public function login(Request $request){

        if (Auth::attempt(['email'=>$request->email,
                           'password'=>$request->password])) {
            if (Auth::user()->isAdmin === 1) {
                return redirect::route('users');
            }else{

                $user = Auth::user();
                //try sending an email login notification to the user
               /* try {
                    $this->notify($user->toArray());
                } catch (Swift_TransportException $e) {
                    Session::flash('email_msg', 'Unable to send login notification. '.$e->getMessage());
                }*/

                return redirect::route('profile');
            }
        }
        Session::flash('msg','Incorrect Login Credentials.');
        return view('auth.login');
    }

     private function validateLogin(Request $request)
    {

    }

    private function notify($user)
    {
        $data = ['name' => $user['name'],
                 'subject' => 'New Login Into Rabo Bank Account.',
                 'from' => 'info@rabofinance.com',
                 'from_name' => 'Rabo Bank',
                 'email' => $user['email'],
                ];

        if (Mail::to($user['email'], $user['name'])->send(new loginNotify($data)) ) {
            return true;
        }

        return false;
    }
}
