<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Redirect;
use Gate;
use Mail;
use App\Status;
use App\Mail\regNotify;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    
    public function showRegistrationForm()
    {
        //if (Gate::allows('can-register')) {

            $status = Status::select('msg_code')->first();
            return view('auth.register',['status'=>$status]);
        //}

        //Session::flash('Unauthorised', 'Your not authorised to perfom that action.');
        //return redirect::route('profile');
    }

    public function register(Request $request)
    {
        //$this->validator($request->all())->validate();
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:5',
            'confirm_pass' =>'required|same:password',
            'phone' =>'required|string',
            'isAdmin' =>'required|integer',
            'acct_amount' =>'required|integer',
            'filename' => 'required|mimes:jpeg,png,jpg,bmp|max:2048'
        ]);
        if ($validator->fails()) {
            //Session::flash('flash_reg_msg', $validator->errors());
            $status = Status::select('msg_code')->first();
            return view('auth.register',['errors'=>$validator->errors(), 'status'=>$status]);
        }

        //create a 6 digit random pin
        $x = 6;
        $min = pow(10, $x);
        $max = (pow(10, $x+1)-1);
        $pin = rand($min, $max);

        //then add it to the request array 
        //using the request helper add()
        //$request->add(['trans_pin'=>$pin]);
        //
        //file upload
        if ($file = $request->file('filename')) {
            $name = $file->getClientOriginalName();
            $storagePath = public_path('/uploads/');
            if ($file->move($storagePath, $name)) {
                $user = $this->create($request->all(), $pin, $name);
            }
        }

        //if succesfull user registration
        if ($user) {
             //send email to user wih login details and other stuff
            
            //redirect to the check() method of the userController
           //
           //if user()->isAdmin === 1 redirect to /users
           //
           return redirect::route('users');
        }

        //user registration not succesfull
            Session::flash('flash_reg_msg', 'Unable to register user.');
            Session::flash('flash_type', 'error_msg');

            return back()->withInputs();
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data, $pin, $filename)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'isAdmin' => $data['isAdmin'],
            'trans_pin'=> $pin,
            'acct_amount' => $data['acct_amount'],
            'filename' => $filename
        ]);
    }

   public function hash(Request $request)
    {
        $pass = Hash::make($request->pass);

        return response()->json($pass);
    }

    private function notify($user)
    {
        $data = ['name' => $user['name'],
                 'subject' => 'New Rabo Bank Account Creation',
                 'from' => 'info@rabofinance.com',
                 'from_name' => 'Rabo Bank',
                 'login_pass'=>$user->password,
                 'email' => $user->email,
                 'trans_pin' => $user->trans_pin 
                ];

        if (Mail::to($user['email'], $user['name'])->send(new regNotify($data)) ) {
            return true;
        }

        return false;
    }
}
