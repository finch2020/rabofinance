<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Session;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(Request $request)
    {
        return view('index');
    }

    public function showLoginForm(Request $request)
    {
        return view('auth.login');
    }


    public function login(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ];
        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            Session::put('red',1);
            return redirect()->back()->withErrors($validation->errors());
        } else {
            if (Auth::attempt(['email' => $request->email,'password' => $request->password])){
                if (Auth::user()->isAdmin === 1) {
                    return redirect()->route('users');
                } else {
                    $user = Auth::user();
                    //try sending an email login notification to the user
                    /* try {
         $this->notify($user->toArray());
     } catch (Swift_TransportException $e) {
         Session::flash('email_msg', 'Unable to send login notification. '.$e->getMessage());
     }*/

                    return redirect()->route('profile');
                }
            }else{
                Session::put('red', 1);
                return redirect()->back()->withErrors(['password' => 'Password is Incorrect'])->withInput();
            }

        }
    }
}
