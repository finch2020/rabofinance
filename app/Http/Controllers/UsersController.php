<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use Session;
use Auth;
use Gate;
//use Illuminate\Support\Facades\File;
use App\Status;
use Redirect;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //check if user is authorized using Gate
        if (Gate::allows('view-all-users') ){
            $users = User::orderBy('created_at')->where('isAdmin',0)->get();
            //get the status message in use
            $status = Status::select('msg_code')->first();
            return view('dashboard.index',['users'=>$users, 'status'=>$status]);

        }

            Session::flash('flash_msg', 'You are not authorised to perform that action.');
            Session::flash('flash_type', 'error_msg');
            return redirect::route('profile');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::allows('view-user')) {
            $user = User::findOrFail($id);
            $trans_history = Transaction::where('id', $id)->get();
            $status = Status::select('msg_code')->first();
            return view('dashboard.viewUser',['userInfo'=>$user,
                                              'trans'=>$trans_history,
                                              'status'=>$status]);
        }

        Session::flash(['flash_msg', 'You are not authorised to perform that action.']);
        Session::flash(['flash_type', 'error_msg']);
        return redirect::route('/check');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('edit-user')) {
            $user = user::findOrFail($id);
            $status = Status::select('msg_code')->first();
            return view('dashboard.edit',['user'=>$user, 'status'=>$status]);
        }

        Session::flash(['flash_msg', 'You are not authorised to perform that action.']);
        Session::flash(['flash_type', 'error_msg']);
        return redirect::route('/check');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($request->hasFile('filename')) {
            $rules = array(['name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255|unique:users',
                        'phone' => 'required|integer',
                        'isAdmin' => 'required|integer',
                        'acct_amount' => 'required|integer',
                        'filename' => 'required|mimes:jpeg,png,jpg,bmp|max:2048'
                    ]);
        }else{

            $rules = array(['name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255|unique:users',
                        'phone' => 'required|integer',
                        'isAdmin' => 'required|integer',
                        'acct_amount' => 'required|integer',
                        'oldFile' => 'required|mimes:jpeg,png,jpg,bmp|max:2048'
                    ]);

        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            Session::flash('flash_msg', 'Unable to update user.');
            Session::flash('flash_type', 'error_msg');
            return view('dashboard.edit',['errors'=>$validator->errors()]);

        }
            if ($request->hasFile('filename')) {

                if ($file = $request->file('filename')) {
                    $name = $file->getClientOriginalName();
                    $storagePath = public_path('/uploads/');
                        if ($file->move($storagePath, $name)) {

                                $user->name = $request->name;
                                $user->email = $request->email;
                                $user->phone = $request->phone;
                                $user->isAdmin = $request->isAdmin;
                                $user->trans_pin = $request->trans_pin;
                                $user->acct_amount = $request->acct_amount;
                                $user->filename = $name;

                                $user->save();
                        }
                }

            }else{
                                $user->name = $request->name;
                                $user->email = $request->email;
                                $user->phone = $request->phone;
                                $user->isAdmin = $request->isAdmin;
                                $user->trans_pin = $request->trans_pin;
                                $user->acct_amount = $request->acct_amount;
                                $user->filename = $request->oldFile;

                                $user->save();
            }

            Session::flash('flash_msg', 'Client Account successfully updated.');
            Session::flash('flash_type', 'success_msg');

            return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('delete-user')) {
            $user = User::findOrFail($id);

            if ($user->delete()) {
                Session::flash('flash_msg', 'User successfully deleted.');
                Session::flash('flash_type', 'success_msg');

                return redirect('/users');
            }else{
                Session::flash('flash_msg', 'Unable to delete user.');
                Session::flash('flash_type', 'error_msg');

                return redirect::route('/users');
            }
        }

        Session::flash('flash_msg', 'You are not authorised to perform that action.');
        Session::flash('flash_type', 'error_msg');
        return redirect::route('/profile');
    }



    public function profile()
    {
        //get the currently logged in user id
        //and use it to get the info from he db
         $user_id = Auth::user()->id;

         $userInfo = User::findOrFail($user_id);

         //get transaction history from the transaction table
         //for this particular user
         $trans_history = Transaction::select('trans_amount', 'receiver_acct_no', 'receiver_bank',                                  'created_at', 'trans_pin','f_name', 'l_name','trans_status')
                                      ->where('user_id', $user_id)
                                      ->latest()
                                      //->orderBy('created_at','desc')
                                      ->take(4)
                                      ->skip(0)
                                      ->get();
         return view('profile.index',['userInfo'=>$userInfo,
                                      'trans'=>$trans_history,
                                  ]);
    }

    public function logout()
    {
        if (Auth::logout() ) {

            return redirect()->route('login');
        }

        Session::flash('flash_msg', 'Can not logout');
        return redirect::route('profile');
    }

    public function contact()
    {
      Session::flash('flash_msg','Thanks for the reach out. We will get back to you in no time.');

      return redirect::route('welcome').'#contact';
    }
}
