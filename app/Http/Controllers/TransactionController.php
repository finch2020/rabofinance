<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;
use App\Status;
use Redirect;

class TransactionController extends Controller
{
     //GET request route resource
    public function transfer()
    {
        $user = Auth::user();
        return view('transfer.form',['userInfo'=>$user, 'balance'=>$user->acct_amount]);
    }
//
//
//RE-RUN TRANSACTIONS MIGRATION (CHANGE TABLE NAME TO 'TRANSACTIONS')
//
//
    //POST request to save transaction details
    public function save(Request $request)
    {
        //
        //Get the currently logged in user instance
        $user = Auth::user();

        //deduct the transfered amount 
        //from the user's account total
        $res = ($user->acct_amount - $request->trans_amount);
        

        //making sure both the creation of the transaction and 
        //the update of the deduction happen
        
        //get current status and use switch statement to set the status to be saved
            $code = Status::select('msg_code')->first();
        

            switch ($code->msg_code) {
                case '0':
                    $status_msg = 'Successfull';
                    break;

                case '1':
                    $status_msg = 'Pending';
                    break;

                    case '2':
                        $status_msg = 'Tax code request';
                        break;

                    case '3':
                        $status_msg = 'Waiting';
                        break;

                    case '4':
                        $status_msg = 'On hold';
                        break;

                    case '5':
                        $status_msg = 'Remita number request';
                        break;
                
                default:
                        $status_msg = 'Successfull';
                    break;
            }

        if (Transaction::create(['user_id'=>$user->id,
                                 'trans_amount'=>$request->trans_amount,
                                 'receiver_acct_no'=>$request->receiver_acct_no,
                                 'receiver_bank'=>$request->receiver_bank,
                                 'trans_pin'=>$request->trans_pin,
                                 'f_name'=>$request->f_name,
                                 'l_name'=>$request->l_name,
                                 'trans_status'=>$status_msg,
                                 'sender_name'=>$user->name
                             ]) && 
            User::where('id', $user->id)->update(['acct_amount'=>$res])) {

            //use switch statement again to set the appropriate flash messge 
            ////to be shown to the user
            switch ($code->msg_code) {
                case '0':
                    Session::flash('flash_msg', 'Transfer successfull.');
                    break;

                case '1':
                    Session::flash('flash_msg', 'Transfer pending, will be confirmed soon.');
                    break;

                    case '2':
                        Session::flash('flash_msg', 'Transfer waiting...Please input tax code.');
                        break;

                    case '3':
                        Session::flash('flash_msg', 'Transfer waiting...');
                        break;

                    case '4':
                        Session::flash('flash_msg', 'Transfer on hold.');
                        break;

                    case '5':
                        Session::flash('flash_msg', 'Please input Remita Number.');
                        break;
                
                default:
                        Session::flash('flash_msg', 'Transfer successfull.');
                    break;
            }

            return redirect::route('profile');
        }

        Session::flash('flash_msg', 'Transfer failed.');
        //Session::flash_type(['flash_type', 'error_msg']);

        return redirect::route('profile');
        
    }

    public function all()
    {
        $user_id = Auth::user()->id;
        $trans_history = Transaction::select('trans_amount', 'receiver_acct_no', 'receiver_bank',                                  'created_at', 'trans_pin','f_name', 'l_name','trans_status')
                                      ->where('user_id', $user_id)
                                      ->latest()
                                      ->paginate(10);
                                      //->get();

        return view('transfer.all',['trans'=>$trans_history]);
    }

    public function all_trans()
    {
        $code = Status::select('msg_code')->first();
        $trans_history = Transaction::select('id','trans_amount', 'receiver_acct_no', 'receiver_bank',                                  'created_at', 'trans_pin','f_name', 'l_name', 'sender_name','trans_status')
                                      ->latest()
                                      ->get();
        //get 

        return view('dashboard.transactions',['trans'=>$trans_history, 'status'=>$code]);
    }

    public function change_status(Request $request)
    {
        $id = $request->id;
        $status = str_replace('-', ' ', $request->status);
        Transaction::where('id', $id)->update(['trans_status'=>$status]) ? Session::flash('flash_msg','Transaction Status Updated!'):Session::flash('flash_msg','Unable to Update Transaction Status');

        return redirect::route('transactions');
    }
}
