<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['user_id', 'trans_amount', 'receiver_acct_no',
    					   'receiver_bank', 'trans_pin', 'f_name', 'l_name','trans_status', 'sender_name'];
    protected $dates = ['created_at'];
}
