<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Redirect;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        //create a 6 digit random pin
        $x = 6;
        $min = pow(10, $x);
        $max = (pow(10, $x+1)-1);
        $pin = rand($min, $max);

        //then add it to the request array 
        //using the request helper add()
        $request->add(['trans_pin'=>$pin]);

        $user = $this->create($request->all());

        //if succesfull user registration
        if ($user) {
            //send email to user wih login details and other stuff
            
            Session::flash('flash_message', 'User has been registered.');
            Session::flash('flash_type', 'success_msg');

            //redirect to the check() method of the userController
           //return redirect::route('/profile');
           return response()->json('Success!');
        }

        //user registration not succesfull
            Session::flash('flash_message', 'Unable to register user.');
            Session::flash('flash_type', 'error_msg');

            //return back()->withInputs();
            return response()->json('Failed');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
